## Bias and Refinement of Multiscale Mean Field Models

This repository contains:

* the code to generate the plots used in the `csma_example` directory
* the tex file and additional resources to compile the paper in the `latex` directory

### Numerical implementation: 

In order for the code to be accessible we provide a jupyter notebook (`csma_example/Plots.ipynb`) 
which can be used specify different csma setups, load simulations and compute the approximation as well as refinement terms. 
The file `csma_example/additional_functions/jitted_functions.py` provides a compiled function to run simulations. 
Examples on how to use these are provided. The functions require the `numba` library to be installed. 
