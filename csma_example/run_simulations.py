# File to run and save steady state simulations for the csma model

import numpy as np
from additional_functions.jitted_functions import fast_simu
from numba import jit

from generic_csma import symbolic_CSMA

import time
import os

from copy import copy

simu_dir = 'saved_simulations/'
det_values_dir = 'saved_det_values/'

@jit
def calc_average(X, T):
    """Generates a sample of E[X] in steady-state.
    The expectation is computed by performing one simulation from
    t=0 to t=time and averaging the values over time/4 .. time
    (the implicit assumption is that the system should be roughly
    in steady-state at time/4).
    """
    n = int(T.shape[0])
    n2 = int(n / 10)

    avg_X = np.zeros(shape=(X.shape[1], X.shape[2]))
    for c in range(X.shape[1]):
        for b in range(X.shape[2]):
            avg_X[c,b] = np.sum(np.diff(T[int(n2):n]) * X[int(n2):n - 1, c, b]) / (T[n - 1] - T[int(n2)])

    return avg_X

def simu_wrapper(csma_model, N, steps):
    x0 = csma_model.initial_state_x
    y0 = csma_model.initial_state_y

    nu = csma_model.nu
    mu = csma_model.mu
    _lambda = csma_model._lambda

    G = csma_model.G

    buffer_size = csma_model.buffer_size
    classes = csma_model.classes


    T_sim, (X_sim, Y_sim) = fast_simu(x0, y0, nu, mu, _lambda, N, G, buffer_size, classes, steps=steps, seed_nr=-1, verbose=False)
    T_sim = np.array(T_sim, dtype=np.float_)
    return T_sim, (X_sim, Y_sim)

def run_save_simus(csma , N, nr_simus):
    dict_simu_jit = {}
    dict_avg_X_jit = {}

    file = None
    for file in os.listdir(simu_dir):
        if file[-4:] == '.npy':
            data = np.load(simu_dir + file, allow_pickle=True).item()


            ## check if save file already exists
            specs = data['specs']
            if np.array_equal(specs['G'], csma.G):
                if np.array_equal(specs['lambda'], csma._lambda):
                    if np.array_equal(specs['mu'], csma.mu):
                        if np.array_equal(specs['nu'], csma.nu):
                            if np.array_equal(specs['buffer_size'], csma.buffer_size):
                                if np.array_equal(specs['N'], N):
                                    print('Existing save file found!')
                                    break
        # set file to non, no file with coinciding specs was found
        file = None

    if file is not None:
        simu_keys = copy(list(data.keys()))
        simu_keys.remove('specs')
        max_ind = max(simu_keys) + 1
    else:
        max_ind = 0

    if file is None:  # in case there are no simulations for this setup
        data = {}
        data['specs'] = {'G': csma.G, 'lambda': csma._lambda, 'mu': csma.mu, 'nu': csma.nu,
                         'buffer_size': csma.buffer_size, 'N': N}
        file = '{}_node_N_{}_csma_{}.npy'.format(csma.G.shape[0], N,
                                                str(time.gmtime().tm_hour) + str(time.gmtime().tm_min) +
                                                str(time.gmtime().tm_sec))

    print('running simus (N={}): '.format(N), end='', flush=True)
    for _i in range(nr_simus):
        # add seed??!!
        seed = max_ind + _i
        print(str(_i) + ', ', end='', flush=True)
        T, (X, Y) = simu_wrapper(csma, N, steps=1e7)
        T = T[:-1]  # get simulation values
        avg_X = calc_average(X, T)
        data[max_ind + _i] = avg_X

    print()

    np.save(simu_dir + file, data)

def load_or_run_approx(csma, dir=det_values_dir, equi_time=150):
    # TODO: finish
    dict_simu_jit = {}
    dict_avg_X_jit = {}

    file = None
    for file in os.listdir(dir):
        if file[-8:] == '_det.npy':
            data = np.load(dir + file, allow_pickle=True).item()


            ## check if save file already exists
            specs = data['specs']
            if np.array_equal(specs['G'], csma.G):
                if np.array_equal(specs['lambda'], csma._lambda):
                    if np.array_equal(specs['mu'], csma.mu):
                        if np.array_equal(specs['nu'], csma.nu):
                            if np.array_equal(specs['buffer_size'], csma.buffer_size):
                                    print('Existing save file found! - No computations needed')
                                    break

        # set file to non, no file with coinciding specs was found
        file = None

    if file is not None:
        v = data['v']
        x_equi = data['x_equi']
        s = data['s']
        t = data['t']
        w = data['w']
        u = data['u']

        print('\n ** Computations Loaded ** \n')
        return x_equi, v, s, t, w, u

    else:  # in case there are no computations for this setup
        print('Running computations ...\n')

        data = {}
        data['specs'] = {'G': csma.G, 'lambda': csma._lambda, 'mu': csma.mu, 'nu': csma.nu,
                         'buffer_size': csma.buffer_size}
        file = '{}_node_csma_{}_det.npy'.format(csma.G.shape[0],
                                                str(time.gmtime().tm_hour) + str(time.gmtime().tm_min) +
                                                str(time.gmtime().tm_sec))

        _, X = csma.ode(time=equi_time)
        x_equi = X[-1]
        v, s, t, w, u = csma.compute_refinements(x_equi)

        data['v'] = v
        data['x_equi'] = x_equi
        data['s'] = s
        data['t'] = t
        data['w'] = w
        data['u'] = u



        np.save(dir + file, data)
        print('\n ** Computations Saved ** \n')
        return x_equi, v, s, t, w, u


if __name__ == '__main__':
    ## specify the model



    # G = np.array([[0, 1],
    #               [1, 0]])
    #
    # buffer_size = 10
    #
    # _lambda = np.array([0.4, 0.2])
    # mu = np.array([2.0, 2.0])
    # nu = np.array([1.2, 2.0])

    # G = np.array([[0, 1, 0],
    #               [1, 0, 1],
    #               [0, 1, 0]])
    #
    # _lambda = np.array([0.5, 0.7, 0.7])
    # mu = np.array([3., 3., 2.])
    # nu = np.array([4., 3., 3.])
    #
    # buffer_size = 10

    #
    # G = np.array([[0, 1, 1, 0],
    #               [1, 0, 0, 1],
    #               [1, 0, 0, 1],
    #               [0, 1, 1, 0]])  # cyclic graph structre
    #
    # _lambda = np.array([1., 1.7, 1.7, 1.5])
    # mu = np.array([10, 10, 10, 10])
    # nu = np.array([3, 3, 3, 3])
    # buffer_size = 10
    #
    # G = np.array([[0, 1],
    #               [1, 0]])
    # _lambda = np.array([0.4, 0.2])
    # nu = np.array([1.2, 2.])
    # mu = np.array([2., 2.])
    # buffer_size = 10

    # cyclic graph
    G = np.array([[0, 1, 0, 0, 1],
                  [1, 0, 1, 0, 0],
                  [0, 1, 0, 1, 0],
                  [0, 0, 1, 0, 1],
                  [1, 0, 0, 1, 0]])
    _lambda = np.array([0.5, 0.7, 0.7, 0.6, 0.4])
    nu = np.array([4, 3, 3, 3, 3])
    mu = np.array([3, 3, 2, 4, 2])
    buffer_size = 10

    # G = np.array([[0, 1, 0, 0, 0, 1],
    #               [0, 0, 0, 0, 0, 1],
    #               [0, 1, 0, 0, 0, 1],
    #               [0, 1, 0, 0, 0, 1],
    #               [0, 1, 0, 0, 0, 1],
    #               [0, 1, 0, 0, 0, 0]])
    #
    # _lambda = np.array([0.4] * G.shape[0])
    # nu = np.array([3.] * G.shape[0])
    # mu = np.array([3.] * G.shape[0])
    # buffer_size = 6

    # G = np.array([[0, 1, 0],
    #               [1, 0, 1],
    #               [0, 1, 0]])
    # _lambda = np.array([1, 1, 1])
    # nu = np.array([3, 3., 3])
    # mu = np.array([3, 3, 3])
    # buffer_size = 10

    # G = np.array([[1]])
    # _lambda = np.array([1])
    # nu = np.array([4])
    # mu = np.array([3])
    # buffer_size = 10


    csma = symbolic_CSMA(nu, mu, _lambda, G, buffer_size)

    # np.save(simu_dir + 'two_node_.npy', data)
    for i in range(5):
        for N in [1]:
            run_save_simus(csma, N=N, nr_simus=1)

    # x_equi, v, s, t, w, u = load_or_run_approx(csma)

    print()

    # print(exists(simu_dir + 'test.npy'))
    # print(os.listdir(simu_dir))





