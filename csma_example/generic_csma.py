"""
The following code was used to compute the results of the paper
Bias and Refinement of Multiscale Mean Field Models by S. Allmeier and N. Gast.

The code provides an implementation of the CSMA model and corresponding mean field and refined mean field approximation.

The code is distributed under the MIT License. For more information see
https://gitlab.inria.fr/sallmeie/bias-and-refinement-of-multiscale-mean-field-models/-/blob/main/LICENSE
"""

# --------------------
# imports
# --------------------

# library for symbolic differentiation
from sympy import symbols, utilities, zeros, Matrix, diff, MutableDenseNDimArray, derive_by_array, \
    ImmutableDenseNDimArray, simplify

# standard numpy and math libraries
import numpy as np
from copy import copy

# libraries for numerical computation
import scipy.integrate as integrate
from scipy.linalg import solve_continuous_lyapunov, solve_sylvester, inv
from scipy.interpolate import interp1d

# library for progress bars
from tqdm import tqdm

# --------------------
# Line Profiler Supplement
# --------------------

# The next lines of code are necessary to run the script using python without removing the '@profile' wrapper.
# The profiler used to analyse the computation times is "Line_Profiler" - "https://github.com/pyutils/line_profiler"
import builtins
try:
    builtins.profile
except AttributeError:
    # No line profiler, provide a pass-through version
    def profile(func): return func
    builtins.profile = profile

# --------------------

class symbolic_CSMA():
    # simple model with only one node / one class
    def __init__(self, nu, mu, _lambda, G, buffer_size):
        print('# --------------------\nInitializing model and variables.\n# --------------------')
        """The following code and included functions need to be modified of one wants use the implementation for any 
        other two timescale model other than the CSMA model. 
        The lines provide possible states of the fast y process, a mapping between the y states and generic integer 
        values. Furthermore, the steady state probabilities are provided. For the csma model these are provided in 
        closed form however, other computational methods could be provided. 

        *Note* -- The class functions: .get_independent_sets(G), .initialize_symbols(), 
        get_pi_activity(self.G, self.activation_factor), .get_transitions_from_current_state(self, y, x=None) 
        have to be properly modified for any adaptation.
        """
        # initialization of the model
        self.nu = nu
        self.mu = mu
        self._lambda = _lambda
        self.buffer_size = buffer_size
        self.G = G
        self.classes = np.array(nu).shape[0]

        # initialization transition and transition rate arrays
        self.transitions = []
        self.transition_rates = []

        # specify initial state (no jobs in the system)
        # this can be modified by manipulating 'model_instance'.initial_state_x = ...
        self.initial_state_x = np.zeros(shape=(self.classes, buffer_size))
        self.initial_state_y = [0] * self.classes

        # get valid states
        self._valid_y_states = self.get_independent_sets(G)
        # define integer to y state mapping
        # (these dicts map the actual tuples / states to integers and vise versa,
        # not the symbols representing the y states)
        self.state_int_map = dict([(state, i) for i, state in enumerate(self._valid_y_states)])
        # define y state to integer mapping
        self.int_state_map = dict([(i, state) for i, state in enumerate(self._valid_y_states)])
        # initialize symbols and symbolic activation factor
        self.initialize_symbols()
        self.activation_factor = [(self.nu[c] / self.mu[c]) * (1 - (1 - self.x[c, 0])) for c in range(self.classes)]
        self.pi = self.get_pi_activity(self.G, self.activation_factor)

        # --------------------
        # the remaining part of the initialization should be generic
        # --------------------
        # initialize other technical variables used for computations
        # K^+ matrix
        self._Kplus = None
        self._Kplus_symbolic = None

        # symbolic representation of the fast system transition matrix
        self._K = None
        # symbolic representation of the avg. drift derivative
        self._d_avg_drift_symbolic = None
        # symbolic representation of the average drift
        self._avg_drift = None
        # non-symbolic representation of the average drift
        self._ode_avg_drift = None
        # lambdified dirft versions (faster computation / evaluation), without symbols
        self._lambdified_drift_dict = None

        # some values to speed up computation

        self.PI = Matrix([self.pi] * len(self.pi))
        self.PI_lambdified = utilities.lambdify([self.x], self.PI, 'numpy')

        # empty variables to be filled during the computation of the refinement terms
        self.last_x = None
        self.dict_grad_F = None
        self._K_plus_PI = None

    def initialize_symbols(self):
        """Initialize sympy symbols and int_symbol mapping for automated differentiation.
        This is similar to the mapping provided in the initialization only here we consider sympy symbols instead of
        tuples.
        """
        self.x = np.array([[symbols('x_{}_{}'.format(c, i)) for i in range(self.buffer_size)] for c in
                           range(self.classes)])
        self.y = np.array([symbols('y_{}'.format(c)) for c in range(self.classes)])
        # define integer to y state symbols mapping
        self.int_symbol_map = {}
        self.state_symbol_map = {}
        y_states = []
        # define the mapping between symbols and integers
        for i, state in enumerate(self._valid_y_states):
            _symbol = symbols(('y_' + '{}' * self.G.shape[0]).format(*state))
            y_states.append(_symbol)
            self.int_symbol_map[i] = _symbol
            self.state_symbol_map[state] = _symbol

    def valid_backoffs(self, y_state):
        """Compute the valid backoff from current y state.
        valid_transitions[c] == 1 - backoff for class c is possible from current state
        valid_transitions[c] == 0 - backoff for class c is ~not~ possible from current state
        Accesible_states return a list of states that can be accessed from y_state.
        """
        y_state = np.array(y_state)
        blocked = np.matmul(self.G, y_state) > 0
        valid_transitions = np.array([not blocked[c] and not y_state[c] for c in range(self.classes)])
        accessible_states = []
        for i, possible_transition in enumerate(valid_transitions):
            new_state = copy(y_state)
            new_state[i] += possible_transition
            if not (new_state == y_state).all():
                accessible_states.append(new_state)
        return valid_transitions, accessible_states

    # @profile
    def get_transitions_from_current_state(self, y, x=None):
        """Provides all possible transitions and transition rates from the current state. The function takes an explicit
        y state. If the x state is not given, the function will return a symbolic expression which can be evaluated in x.
        :param y: current state of Y representet as a 0/1 list
        :param x: current x state
        :return: transitions, transition_rates - two lists containing transitions and rates for the x, y state
        """

        # initializing parameters and lists
        _lambda = self._lambda
        nu = self.nu
        mu = self.mu
        if x is None:
            x = self.x
        transitions = []
        transition_rates = []
        classes = self.classes
        buffer_size = self.buffer_size

        # arrivals (increase queue length of a random server of class c)
        for c in range(classes):
            for i in range(0, buffer_size):
                # arrival at server of class c with queue size i
                if i == 0:
                    ell = self.e(c, i)
                    rate = _lambda[c] * (1 - x[c, 0])
                    transitions.append([ell, y, y])
                    transition_rates.append(rate)
                elif 0 < i < buffer_size:
                    ell = self.e(c, i)
                    rate = _lambda[c] * (x[c, i - 1] - x[c, i])
                    transitions.append([ell, y, y])
                    transition_rates.append(rate)
                elif i == buffer_size - 1:
                    # no arrival if buffer is full
                    ell = np.zeros(shape=self.e(c, i).shape)
                    rate = _lambda[c] * x[c, i]
                    transitions.append([ell, y, y])
                    transition_rates.append(rate)

        # backoff (reduce queue length of server of class c and activate node)
        valid_transitions, accessible_states = self.valid_backoffs(y_state=y)
        for c in range(classes):
            if valid_transitions[c]:  # check if backoff to node c from current y state is possible
                for i in range(0, buffer_size):
                    # back off transition of server i (class c) to node c
                    ell = - self.e(c, i)
                    y_prime = copy(y)
                    y_prime[c] = 1
                    transition = [ell, y, y_prime]
                    if i == buffer_size - 1:
                        rate = nu[c] * x[c, i]
                    else:
                        rate = nu[c] * (x[c, i] - x[c, i + 1])
                    transitions.append(transition)
                    transition_rates.append(rate)

        # transmission completion y[c] = 1 -> y[c] = 0
        for c in range(classes):
            if y[c]:
                ell = np.zeros(shape=(classes, buffer_size))
                y_prime = copy(y)
                y_prime[c] = 0
                transition = ([ell, y, y_prime])
                rate = mu[c]
                transitions.append(transition)
                transition_rates.append(rate)

        return transitions, transition_rates

    def e(self, c, i):
        """helper function which returns a unit vector
        """
        _e = np.zeros(shape=(self.classes, self.buffer_size))
        _e[c, i] = 1
        return _e

    def drift(self, x, y):
        """Computes and, if x is given, evaluates the drift F (of the slow system)."""

        if not isinstance(y[0], int):
            raise ValueError('Y state has no integer values')
        transitions, transition_rates = self.get_transitions_from_current_state(y)

        _drift = zeros(self.classes, self.buffer_size)

        for i, jump in enumerate(transitions):
            _drift += Matrix(jump[0]) * transition_rates[i]

        if isinstance(x[0, 0], float) or isinstance(x[0, 0], int):
            # if x is a np array evaluate the expression
            sub_dict = {}
            for c in range(self.classes):
                for i in range(len(self.x[0])):
                    sub_dict[self.x[c, i]] = x[c, i]
            _drift = np.array(_drift.evalf(subs=sub_dict)).astype(np.float64)

        return _drift

    def lambdified_drift(self, x, y):
        """A lambdified version of the drift for faster computation.
        """
        # assert isinstance(x[0,0], float) or isinstance(x[0,0], int)
        if self._lambdified_drift_dict is None:
            self._lambdified_drift_dict = {}
            y_states = self._valid_y_states
            for y1 in y_states:
                symbolic_drift = self.drift(self.x, list(y1))
                self._lambdified_drift_dict[y1] = utilities.lambdify([self.x], symbolic_drift, 'numpy')
        # return lambdified version of the drift for (x,y)
        return self._lambdified_drift_dict[tuple(y)](x)

    def avg_drift(self, x):
        """Average drift of the slow system obtained by averaging over the steady state distibution of the fast process.
        """

        if self._avg_drift is None:
            # act_factor = [(self.nu[c] / self.mu[c]) * (1 - (1 - self.x[c,0])) for c in range(classes)]
            # get steady state probabilities and y states
            pi_act, y_states = self.get_pi_activity(self.G, self.activation_factor, return_sets=True)

            # calculate average drift
            _avg_drift = zeros(self.classes, self.buffer_size)
            for i, y in enumerate(y_states):
                _avg_drift += pi_act[i] * self.drift(self.x, list(y))

            _avg_drift = simplify(_avg_drift)
            self._avg_drift = _avg_drift
        else:
            _avg_drift = self._avg_drift

        # evaluate expression if x has values
        if isinstance(x[0, 0], float) or isinstance(x[0, 0], int):
            sub_dict = {}
            for c in range(self.classes):
                for i in range(len(self.x[0])):
                    sub_dict[self.x[c, i]] = x[c, i]
            _avg_drift = _avg_drift.evalf(subs=sub_dict)
            return np.array(_avg_drift).astype(np.float64)
        else:
            # return symbolic expression otherwise
            # evaluated_function = utilities.lambdify([self.x], self., 'numpy')(x)

            return _avg_drift

    def lamdified_avg_drift_for_ode(self, x):
        """Lambdified (see sympy function lambdify) average drift for numerical integration. Decreases computation time.
        """
        if self._ode_avg_drift is None:
            symbolic_avg_drift = self.avg_drift(self.x)
            self._ode_avg_drift = utilities.lambdify([self.x], symbolic_avg_drift, 'numpy')
        return self._ode_avg_drift(x)

    def ode(self, time=20):
        """Integrates the average mean field. Returns the time steps and trajectory.
        """
        number_of_steps = 1000
        T = np.linspace(0, time, number_of_steps)
        print("Integrating MF")
        # Integrate the average drift
        X = integrate.odeint(lambda x, t: self.lamdified_avg_drift_for_ode(x.reshape(self.classes, self.buffer_size)).flatten(),
                             self.initial_state_x.flatten(), T)

        X = X.reshape(number_of_steps, self.classes, self.buffer_size)
        return T, X

    @profile
    def d_avg_drift(self, x):
        """Calculate the (symbolic) derivative of the average drift
        """
        classes = self.classes
        buffer_size = self.buffer_size

        # check if symbolic version has been computed
        if self._d_avg_drift_symbolic is None:
            print('Computing Avg. Drift Derivative')
            # initialize tensor for derivative
            _d_avg_drift = MutableDenseNDimArray(np.zeros(
                shape=(classes, buffer_size, classes, buffer_size),
                dtype=object
            ))

            _avg_drift = ImmutableDenseNDimArray(self.avg_drift(self.x))

            # fill tensor with symbolic derivative
            for c2 in range(classes):
                for x2 in range(buffer_size):
                    _d_avg_drift[:, :, c2, x2] = \
                        np.array(diff(_avg_drift, self.x[c2, x2]))

            # convert to immutable tensor for faster evaluation
            _d_avg_drift = ImmutableDenseNDimArray(_d_avg_drift)

            # save tensor for further use
            self._d_avg_drift_symbolic = _d_avg_drift
        else:
            # use existing computations
            _d_avg_drift = self._d_avg_drift_symbolic

        # if x values are given, evaluate the derivative
        if isinstance(x[0, 0], float) or isinstance(x[0, 0], int):
            sub_dict = {}
            for c in range(self.classes):
                for i in range(len(self.x[0])):
                    sub_dict[self.x[c, i]] = x[c, i]
            evaluated_function = np.array(_d_avg_drift.subs(sub_dict), dtype=np.float64)
            return evaluated_function

        return np.array(_d_avg_drift)

    @profile
    def dd_avg_drift(self, x):
        """Calculate the second derivative of the average drift. The structure follow the code of d_avg_drift closely.
        """
        print('Computing Avg. Drift 2nd Derivative')

        classes = self.classes
        buffer_size = self.buffer_size
        # _dd_avg_drift[i] is the hessian of the i-th drift entry ( \partial^2 f_i / \partial x_j \partial x_k )

        _d_avg_drift = ImmutableDenseNDimArray(self.d_avg_drift(self.x))

        _dd_avg_drift = np.zeros(
            shape=(classes, buffer_size, classes, buffer_size, classes, buffer_size),
            dtype=object
        )

        # initialize progress bar
        pbar = tqdm(total=int(classes * buffer_size))
        # calculate second derivative
        for c3 in range(classes):
            for x3 in range(buffer_size):
                # print('{} / {} ; '.format((c3) * buffer_size + (x3 + 1), classes * buffer_size), end='')
                _dd_avg_drift[:, :, :, :, c3, x3] = \
                    np.array(diff(_d_avg_drift, self.x[c3, x3]))
                pbar.update(1)

        _dd_avg_drift = ImmutableDenseNDimArray(_dd_avg_drift)

        if isinstance(x[0, 0], float) or isinstance(x[0, 0], int):
            sub_dict = {}
            for c in range(self.classes):
                for i in range(len(self.x[0])):
                    sub_dict[self.x[c, i]] = x[c, i]
            evaluated_function = np.array(_dd_avg_drift.subs(sub_dict), dtype=np.float64)
            return evaluated_function
        else:
            return np.array(_dd_avg_drift)

    # @profile
    def avg_q(self, x):
        """Compute the average Q tensor.
        """
        print('Computing Q')

        # Compute y dependent q tensor
        def q(x, y):
            transitions, transition_rates = self.get_transitions_from_current_state(y)

            sub_dict = {}
            for c in range(self.classes):
                for _i in range(len(self.x[0])):
                    sub_dict[self.x[c, _i]] = x[c, _i]

            # get transition rates
            transition_rates = np.squeeze(np.array(Matrix(transition_rates).evalf(subs=sub_dict))).astype(np.float64)

            # initialize tensor
            _q = np.zeros(shape=(self.classes ** 2, self.buffer_size ** 2)).flatten()
            # loop and sum over all transitions
            for _i in range(len(transitions)):
                ell = transitions[_i][0].flatten()  # take only the jumps of the slow component
                _q += np.kron(ell, ell) * transition_rates[_i]

            return _q.reshape(self.x.shape * 2)

        # compute steady-state probabilities
        act_factor = [(self.nu[c] / self.mu[c]) * (1 - (1 - x[c, 0])) for c in range(self.classes)]
        pi_act, y_states = self.get_pi_activity(self.G, act_factor, return_sets=True)

        # compute average q by summing over all y states
        _avg_q = np.zeros(shape=(self.x.shape * 2))
        for i, y in enumerate(y_states):
            _avg_q += pi_act[i] * q(x, list(y))

        return _avg_q

    def K(self):
        """Compute the Transition matrix K for the fast process. The matrix depends on the state of x and can be evaluated
        as it has symbolic representation.
        """
        # compute symbolic representation of K if it has not been done
        if self._K is None:
            # get steady-state states and probabilities
            pi_act, y_states = self.get_pi_activity(self.G, self.activation_factor, return_sets=True)

            _K = zeros(len(y_states), len(y_states))

            # cycle through y states
            for y1 in y_states:
                # get transitions for every y state
                transitions, transition_rates = self.get_transitions_from_current_state(list(y1))
                index1 = self.state_int_map[y1]
                # sum over possible and relevant y transitions
                for i, transition in enumerate(transitions):
                    y2 = tuple(transition[2])
                    if y2 != y1: # check for relevant transitions (
                        # add transition from y1 to y2 to corresponding entry of K (through state index mapping)
                        index2 = self.state_int_map[y2]
                        _K[index1, index2] += transition_rates[i]
                        _K[index1, index1] -= transition_rates[i]
            self._K = _K
            return _K
        else:  # return precomputed symbolic representation of K
            return self._K

    # @profile
    def Kplus(self, x):
        """Computation of K^+, the Matrix used to compute the solution of the Poisson equation.
        """
        # calculate (K + PI)^-1 * ( I + PI )
        _K_plus_PI_inverse = np.array(self.K_PI_numpy_inv(x), dtype=np.float64)

        sub_dict = {}
        for c in range(self.classes):
            for i in range(len(self.x[0])):
                sub_dict[self.x[c, i]] = x[c, i]

        PI_eval = self.PI_lambdified(x)
        _eye = np.eye(_K_plus_PI_inverse.shape[0])

        # computation of K^+ as described in the paper
        _Kplus =  _K_plus_PI_inverse.dot(_eye - PI_eval)
        self._Kplus = _Kplus
        return _Kplus

    # @profile
    def K_PI_numpy_inv(self, x):
        """Computation of the inverse of K+PI to compute the derivative of K^+.
        """
        if self._K_plus_PI is None:
            PI = self.PI
            _K = self.K()
            _K_plus_PI = (_K + PI)
            _K_plus_PI = utilities.lambdify([self.x], _K_plus_PI, 'numpy')
            self._K_plus_PI = _K_plus_PI
        else:
            _K_plus_PI = self._K_plus_PI

        sub_dict = {}
        for c in range(self.classes):
            for i in range(len(self.x[0])):
                sub_dict[self.x[c, i]] = x[c, i]

        # compute inverse using numpy (a lot faster than using sympy)
        numpy_inv = _K_plus_PI(x)
        numpy_inv = inv(
            numpy_inv
        )
        return numpy_inv

    # @profile
    def sol_poisson_fast_drift(self, x, y):
        """Computes the solution of the poisson equation based on K^+ (Kplus).
        input: x -  np vector
        y - integer (see y state <-> integer mapping)
        """
        y_states = self._valid_y_states
        Kplus = self.Kplus(x)
        sol_poiss = 0
        index1 = self.state_int_map[tuple(y)]

        # computation of the solution as given by Lemma 4 (3) of the paper
        for y2 in y_states:
            index2 = self.state_int_map[y2]
            drift = self.lambdified_drift(x, list(y2))
            sol_poiss += Kplus[index1, index2] * drift
        return sol_poiss

    @profile
    def avg_o(self, x):
        """Computation of O bar / average O tensor defined as in the paper.
        """
        print('Computing O')
        # calculate O for sylvester equation
        pi, y_states = self.get_pi_activity(self.G, self.activation_factor, return_sets=True)

        sub_dict = {}
        for c in range(self.classes):
            for b in range(len(self.x[0])):
                sub_dict[self.x[c, b]] = x[c, b]
        pi = np.squeeze(np.array(Matrix(pi).evalf(subs=sub_dict))).astype(np.float64)

        Kplus = self.Kplus(x)

        # helper function to compute inner terms for O
        @profile
        def helper(_x, y):
            sub_dict = {}
            for c in range(self.classes):
                for i in range(len(self.x[0])):
                    sub_dict[self.x[c, i]] = _x[c, i]

            inner_sum = 0
            transitions, transition_rates = self.get_transitions_from_current_state(list(y))
            transition_rates = np.squeeze(np.array(Matrix(transition_rates).evalf(subs=sub_dict))).astype(np.float64)


            for i, transition in enumerate(transitions):
                if np.array_equal(y, transition[1]):
                    [ell, y1, y2] = transition
                    if not np.isclose(transition_rates[i], 0):
                        inner_sum += transition_rates[i] * np.tensordot(
                            np.squeeze(self.sol_poisson_fast_drift(x, y2)), np.array(ell), axes=0)


            return inner_sum.astype(np.float64)

        # compute O
        _O = np.zeros(shape=(self.classes, self.buffer_size, self.classes, self.buffer_size))
        _O = 0

        for i in tqdm(range(len(y_states))): # compute average and display progress bar using tqdm
            _O += np.array(pi[i] * helper(x, y_states[i]))
        return _O

    @profile
    def Kplus_deriv(self, x):
        """Computation of the derivative of K^+
        """

        if self.last_x is None or not np.allclose(self.last_x, x):
            # compute necessary matrices
            _Kplus = self.Kplus(x)
            inverse = self.K_PI_numpy_inv(x)

            # compute
            pi = self.get_pi_activity(self.G, self.activation_factor)
            y_dim = len(pi)

            sub_dict = {}
            for c in range(self.classes):
                for i in range(len(self.x[0])):
                    sub_dict[self.x[c, i]] = x[c, i]

            PI_eval = np.array(ImmutableDenseNDimArray(self.PI).subs(sub_dict), dtype=np.float64)

            PI_sym = self.PI
            grad_Kplus = []

            PI = self.PI
            _K = self.K()
            _K_plus_PI = (_K + PI)

            # compute derivate of K^+ using equation (29) of the paper
            for i in range(len(self.x.flatten())):
                deriv = np.array(diff(_K_plus_PI, self.x.flatten()[i]).evalf(subs=sub_dict)).astype(np.float64)
                deriv_PI = np.array(diff(PI_sym, self.x.flatten()[i]).evalf(subs=sub_dict)).astype(np.float64)
                grad_Kplus_i = - np.dot(inverse, np.dot(deriv, inverse))  # use formula - E^-1 * D_x E * E^-1 = D_x E^-1
                grad_Kplus_i = np.dot(grad_Kplus_i, np.eye(y_dim) - PI_eval) - np.dot(inverse, deriv_PI)
                grad_Kplus.append(
                    grad_Kplus_i
                )

            self.last_x = x
            self._Kplus_deriv = np.array(grad_Kplus, dtype=np.float64).reshape(self.classes, self.buffer_size, y_dim, y_dim)
            return self._Kplus_deriv
        else:
            return self._Kplus_deriv

    def compute_W(self, A, Q):
        """
        Computation of the W correction tensor.
        """
        print('Computing W')
        prod_dims = np.prod(self.x.shape)
        # solve Lyapunov equation to compute W
        return solve_continuous_lyapunov(A.reshape(prod_dims, prod_dims),
                                         -Q.reshape(prod_dims, prod_dims)).reshape(self.x.shape * 2)

    def compute_V(self, A, B, W):
        """Computation of the V correction matrix.
        """
        print('Computing V')
        product_dims = np.prod(self.x.shape)
        # compute inverse of first derivative
        A_inv = inv(A.reshape(product_dims, product_dims)).reshape(self.x.shape * 2)
        # compute tensor product as in section 4.5.1
        inner_tensor_product = np.tensordot(B, W, axes=([2, 3, 4, 5], [0, 1, 2, 3]))
        return -np.tensordot(A_inv, inner_tensor_product, axes=([2, 3], [0, 1])) / 2

    def compute_U(self, A, O):
        """Computation of the U correction tensor.
        """
        print('Computing U')
        # some reshaping to align axes
        dim_product = np.prod(self.x.shape)
        A_transpose = np.transpose(A, axes=(2,3,0,1)).reshape(dim_product, dim_product)
        A = A.reshape(dim_product, dim_product)
        # solve Sylvester Equation
        U = solve_sylvester(A, A_transpose, -O.reshape(dim_product, dim_product))
        U = U.reshape(self.x.shape * 2)
        if len(U.shape) < 2:
            return np.expand_dims(U, axis=0)
        else:
            return U

    def compute_T(self, A, B, U):
        """Computation of the T correction matrix.
        """
        print('Computing T')
        product_dims = np.prod(self.x.shape)
        A_inv = inv(A.reshape(product_dims, product_dims)).reshape(self.x.shape * 2)
        inner_tensor_product = np.tensordot(B, U, axes=([2, 3, 4, 5], [0, 1, 2, 3]))
        return -np.tensordot(A_inv, inner_tensor_product,  axes=([2, 3], [0, 1]))

    @profile
    def helper_S(self, _x, y):
        """Helper function used to compute the refinement / correction term S.
        """

        sub_dict_2 = {}
        for c2 in range(self.classes):
            for b2 in range(self.buffer_size):
                sub_dict_2[self.x[c2, b2]] = _x[c2, b2]

        Kplus_jac = self.Kplus_deriv(_x)
        inner_sum = np.zeros(shape=self.x.shape)

        pi, y_states = self.get_pi_activity(self.G, self.activation_factor, return_sets=True)

        # get transitions dependent on y
        transitions, transition_rates = self.get_transitions_from_current_state(list(y))
        # evaluate symbolic rates for x
        transition_rates = np.squeeze(np.array(Matrix(transition_rates).evalf(subs=sub_dict_2))).astype(np.float64)

        if self.dict_grad_F is None:
            self.dict_grad_F = {}
            for y in y_states:
                self.dict_grad_F[y] = np.transpose(np.array(derive_by_array(self.drift(self.x, list(y)),
                                                                                     self.x).subs(sub_dict_2),
                                                                     dtype=np.float64), axes=[2, 3, 0, 1])

        for y3 in y_states:
            # get the drift for y'' (y3)
            drift = self.lambdified_drift(_x, y3)
            # drift = np.array(self.drift(_x, list(y3))).astype(np.float64)

            grad_F = self.dict_grad_F[y3]

            for i in range(len(transitions)):
                ell = transitions[i][0]
                y2 = transitions[i][2]
                if not np.isclose(transition_rates[i], 0.):

                    for c1 in range(self.classes):
                        for b1 in range(self.buffer_size):
                            index_y3 = self.state_int_map[tuple(y3)]
                            index_y2 = self.state_int_map[tuple(y2)]
                            inner_sum[c1, b1] += transition_rates[i] * \
                                                 (
                                                         drift[c1, b1] * np.dot(
                                                     Kplus_jac[:, :, index_y2, index_y3].flatten(),
                                                     ell.flatten()
                                                 ) +
                                                         self._Kplus[index_y2, index_y3] * np.dot(
                                                     grad_F[c1, b1].flatten(), ell.flatten())
                                                 )
        return inner_sum.astype(np.float64)

    @profile
    def compute_S(self, A, x):
        """
        Computation of the refinement term S.
        """
        print('Computing S')
        # F and dF are the drift and derivative (w.r.t x) not! the derivatives of the average drift
        # very model dependent... need transitions to compute quantity
        pi, y_states = self.get_pi_activity(self.G, self.activation_factor, return_sets=True)

        sub_dict = {}
        for c in range(self.classes):
            for b in range(len(self.x[0])):
                sub_dict[self.x[c, b]] = x[c, b]

        pi = np.squeeze(np.array(Matrix(pi).evalf(subs=sub_dict))).astype(np.float64)

        outer_sum = 0
        for i in tqdm(range(len(y_states))):
            outer_sum += np.array(pi[i] * self.helper_S(x, y_states[i]))

        # outer_sum = np.sum(np.array([pi[i] * self.helper_S(x, y) for i, y in enumerate(y_states)]), axis=0)

        return - np.tensordot(inv(A.reshape(np.prod(self.x.shape), np.prod(self.x.shape))).reshape(self.x.shape * 2),
                              outer_sum, axes=([2,3],[0,1]))

    def get_pi_activity(self, G, act_factor, return_sets=False, normalize=True):
        """Computes steady state probabilities accessible states of the node vector (y-states)
        G - Graph as np array
        act_factor - activity factor equal to nu/mu * (1 - (1-x[0]))
            - using our state representation (1-x[0]) is the number of idle servers
        return_sets return steady state probabilities and corresponding states of y
        """
        ind_sets = list(self.get_independent_sets(G))
        ind_sets = sorted(ind_sets, key=np.sum)
        pi_act = []

        for s in ind_sets:
            aux_prod = 1
            for c in range(G.shape[0]):
                if s[c]:
                    aux_prod = aux_prod * act_factor[c]
            pi_act.append(aux_prod)

        if not normalize:
            return pi_act

        if return_sets:
            return np.array(pi_act) / np.sum(pi_act), ind_sets

        return np.array(pi_act) / np.sum(pi_act)

    def get_independent_sets(self, G):
        """Returns a list of accessible states for the node vector y given the node graph structure G."""
        curr_ind = 0
        indep_sets = [tuple([0] * G.shape[0])]
        set_indep_sets = set(indep_sets)

        while curr_ind < len(indep_sets):
            curr_set = list(indep_sets[curr_ind])
            curr_ind = curr_ind + 1
            unblocked = np.matmul(G, curr_set) == 0
            for ind in range(G.shape[0]):
                if unblocked[ind]:
                    aux_set = curr_set.copy()
                    aux_set[ind] = 1
                    if tuple(aux_set) not in set_indep_sets:
                        indep_sets.append(tuple(aux_set))
                        set_indep_sets = set(indep_sets)

        return set(indep_sets)

    def get_pi_back(self, G, act_factor):
        """Return the steady state occupation of y nodes.
        Probably not needed..."""
        ind_sets = list(self.get_independent_sets(G))
        ind_sets = sorted(ind_sets, key=np.sum)

        pi_act = self.get_pi_activity(self.G, act_factor)

        pi_back = np.zeros(G.shape[0])

        for ind, s in enumerate(ind_sets):
            unblocked = np.matmul(G + np.eye(G.shape[0]), s) == 0
            for c in range(G.shape[0]):
                if unblocked[c]:
                    pi_back[c] += pi_act[ind]

        return pi_back

    def simulate(self, N, time=20):
        """Simple simulation implementation. For steady state / long trajectories the jitted functions should be used.
        These function can be found
        """
        t = 0.
        x = copy(self.initial_state_x)
        y = copy(self.initial_state_y)

        time = N * time  # time scaling

        times = []
        states_x = []
        states_y = []
        times.append(copy(t))
        states_x.append(copy(x))
        states_y.append(copy(y))


        # possible_y_states = self.get_independent_sets(self.G)
        # possible_y_states = [list(y) for y in possible_y_states]

        while t < time:
            transitions, transition_rates = self.get_transitions_from_current_state(y, x=x)

            rates_sum = sum(transition_rates)
            a = np.random.random() * rates_sum

            for i, transition in enumerate(transitions):
                if a > transition_rates[i]:
                    a -= transition_rates[i]
                else:
                    break

            x += transition[0] / N  # add \ell to the current state
            y = transition[2]  # change y to new state

            t += np.random.exponential(1 / rates_sum)

            # t += np.random.exponential(1 / rates_sum)
            times.append(copy(t))
            states_x.append(copy(x))
            states_y.append(copy(y))

        times = np.array(times)
        times /= N
        states_x = np.array(states_x)
        states_y = np.array(states_y)
        return times, (states_x, states_y)

    def calculate_mean_var(self, nr_simus=50, N=30, time=30, time_steps=400):
        """
        Function to compute sample mean and variance from simulations.
        """
        # initialize the mean values
        sim_mean = np.zeros(shape=(time_steps , self.x.shape[0], self.x.shape[1]))
        sim_var = np.zeros(shape=sim_mean.shape)

        # initialize save dictionary
        simulations = {}
        interpolation_times = np.linspace(0, time, time_steps)
        print('Simulating:')
        for i in tqdm(range(nr_simus)):
            # if i % 10 == 0 and i > 0:
            #     print('{} / {}; '.format(i, nr_simus), end='')
            _T, (_X, _Y) = self.simulate(N, time=time)

            # save simulation for variance computation
            simulations[i] = (_T, _X, _Y)

            # get interpolated data from simulation
            interpolation = interp1d(_T, _X, axis=0)

            # add trajectory to mean
            for k, time in enumerate(interpolation_times):
                sim_mean[k] += interpolation(time)  # .transpose()
        # normalize sample mean
        sim_mean /= float(nr_simus - 1)

        # calculate variance
        print('Computing Variance:')
        for i in tqdm(range(nr_simus)):
            for k, time in enumerate(interpolation_times):
                (_T, _X, _Y) = simulations[i]
                interpolation = interp1d(_T, _X, axis=0)
                sim_var[k] += np.square(interpolation(time) - sim_mean[k])
        # normalize sample variance
        sim_var /= float(nr_simus - 1)

        return sim_mean, sim_var, interpolation_times

    @profile
    def compute_refinements(self, X):
        """
        Computes all refinement terms for a given state X. It should be noted that the refinements have to be manually
        rescaled by a factor of 1/N when compared to values obtained by simulation.

        :param X: a state of the model for which to compute the refinement terms
        :return: first order refinement terms v,s,t and second order refinement terms w, u
        """
        print('---------\nComputing Refinements:\n')
        print('\tAverage Drift Derivatives:\n')
        a = self.d_avg_drift(X)
        b = self.dd_avg_drift(X)

        print('\n\tClassical Refinements:\n')
        q = self.avg_q(X)
        w = self.compute_W(a, q)
        v = self.compute_V(a, b, w)

        print('\n\tTwo-Time Scale Refinements:\n')
        o = self.avg_o(X)
        u = self.compute_U(a, o)
        t = self.compute_T(a, b, u)
        s = self.compute_S(a, X)

        return v, -s, -t, w, u


if __name__ == "__main__":
    # example model rates and adjacency matrix are taken from Dissertation Cecchi - p. 76
    G = np.array([[0, 1, 1, 1, 0],
                  [1, 0, 0, 0, 0],
                  [1, 0, 0, 0, 1],
                  [1, 0, 0, 0, 1],
                  [0, 0, 1, 1, 0]])
    _lambda = np.array([0.5, 0.7, 0.7, 0.6, 0.4])
    nu = np.array([4, 3, 3, 3, 3])
    mu = np.array([3, 3, 2, 4, 2])
    buffer_size = 10

    # compute mean field and refinement
    csma = symbolic_CSMA(nu, mu, _lambda, G, buffer_size)

    T, X = csma.ode(time=150)

    v, s, t, w, u = csma.compute_refinements(X[-1])

