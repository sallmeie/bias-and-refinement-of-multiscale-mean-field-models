from numba import jit, njit
import numpy as np
from copy import copy
from numba.typed import List

from numpy import empty

@jit(nopython=True)
def e(c,i, classes, buffer_size):
    _e = np.zeros(shape=(classes, buffer_size), dtype=np.float_)
    _e[c, i] = 1
    return _e

# @jit
def get_transitions(x,y, nu, mu, _lambda, classes, buffer_size, G):
    # _lambda = _lambda
    # nu = nu
    # mu = mu

    valid_transitions = valid_backoffs(y_state=y, G=G)
    nr_transitions = classes * buffer_size + valid_transitions.sum() * buffer_size + y.sum()
    nr_transitions = int(nr_transitions)

    transitions_x = np.empty(shape=(nr_transitions, classes, buffer_size), dtype=np.float_)
    transitions_y_from = np.empty(shape=(nr_transitions, classes), dtype=np.float_)
    transitions_y_to = np.empty(shape=(nr_transitions, classes), dtype=np.float_)
    transition_rates = np.empty(shape=(nr_transitions), dtype=np.float_)

    ind = 0

    # arrivals (increase queue length of a random server of class c)
    for c in np.arange(classes):  # c * buffersize transitions
        for i in np.arange(0, buffer_size):
            # arrival at server of class c with queue size i
            if i == 0:
                ell = e(c, i, classes, buffer_size)
                rate = _lambda[c] * (1 - x[c, 0])
                transitions_x[ind] = ell
                transitions_y_from[ind] = y
                transitions_y_to[ind] = y
                transition_rates[ind] = rate
                ind += 1
            elif 0 < i < buffer_size:
                ell = e(c, i, classes, buffer_size)
                rate = _lambda[c] * (x[c, i - 1] - x[c, i])
                transitions_x[ind] = ell
                transitions_y_from[ind] = y
                transitions_y_to[ind] = y
                transition_rates[ind] = rate
                ind += 1
            elif i == buffer_size - 1:
                # no arrival if buffer is full
                ell = np.zeros(shape=e(c, i, classes, buffer_size).shape, dtype=np.float_)
                rate = _lambda * x[c, i]
                transitions_x[ind] = ell
                transitions_y_from[ind] = y
                transitions_y_to[ind] = y
                transition_rates[ind] = rate
                ind += 1

    # backoff (reduce queue length of server of class c and activate node)
    for c in range(classes):  # valid_transitions.sum() * buffer_size transitions
        if valid_transitions[c]:  # check if backoff to node c from current y state is possible
            for i in range(0, buffer_size):
                # back off transition of server i (class c) to node c
                ell = - e(c, i, classes, buffer_size)
                y_prime = copy(y)
                y_prime[c] = 1
                if i == buffer_size - 1:
                    rate = nu[c] * x[c, i]
                else:
                    rate = nu[c] * (x[c, i] - x[c, i + 1])
                transitions_x[ind] = ell
                transitions_y_from[ind] = y
                transitions_y_to[ind] = y_prime
                transition_rates[ind] = rate
                ind += 1


    # transmission completion y[c] = 1 -> y[c] = 0
    for c in range(classes):  # y.sum() transitions
        if y[c]:
            ell = np.zeros(shape=(classes, buffer_size))
            y_prime = copy(y)  # changed from copy(y) to y
            y_prime[c] = 0
            rate = mu[c]
            transitions_x[ind] = ell
            transitions_y_from[ind] = y
            transitions_y_to[ind] = y_prime
            transition_rates[ind] = rate
            ind += 1

    return transitions_x, transitions_y_from, transitions_y_to, transition_rates

@jit(nopython=True)  # working
def valid_backoffs(y_state, G):
    """valid_transitions[c] == 1 - backoff for class c is possible from current state
    valid_transitions[c] == 0 - backoff for class c is ~not~ possible from current state
    accesible_states return a list of states that can be accessed from y_state
    """
    # y_state = np.array(y_state, dtype=np.float_)
    classes = G.shape[0]

    blocked = np.empty(shape=(int(classes)), dtype=np.float_)
    for c1 in range(classes):  # replaces np.matmul(G, y_state) > 0
        helper = 0
        for c2 in range(classes):
            helper += G[c1,c2] * y_state[c2]
        if helper > 0:
            blocked[c1] = True
        else:
            blocked[c1] = False

    valid_transitions = np.empty(shape=(int(classes)), dtype=np.bool_)
    for c in range(classes):
        valid_transitions[c] = not blocked[c] and not y_state[c]

    # accessible_states = np.empty(shape=(len(valid_transitions), classes), dtype=np.float_)
    # for i, possible_transition in enumerate(valid_transitions):
    #     new_state = y_state
    #     new_state[i] += possible_transition
    #     if not (new_state == y_state).all():
    #         accessible_states[i] = new_state
    return valid_transitions  # , accessible_states

@jit(nopython=True)
def jit_simulate(x0, nu, mu, _lambda, N, G, buffer_size, classes, steps=1e6, seed_nr=-1, verbose=False):
    t = 0.

    x = x0
    y = 0
    time_index = 0
    max_steps = int(steps)
    # initialize vectors
    times = np.empty(shape=(max_steps), dtype=np.float64)
    states = np.empty(shape=(max_steps, buffer_size), dtype=np.float64)
    y_states = np.empty(shape=(max_steps), dtype=np.float64)
    times[time_index] = t
    states[time_index] = x
    y_states[time_index] = y

    if seed_nr != -1:
        np.random.seed(seed_nr)

    time_index += 1
    # simulate the markov chain
    while time_index < max_steps:
        rates_sum = (1-y) * nu * (1-x[0]) + y * mu + _lambda

        # transition selection
        a = np.random.random()


        if a < (1-y) * (nu / rates_sum) * (1-x[0]): # back off can only happen to jobs with queue
            # add a job to one queue
            b = np.random.random()
            m = np.searchsorted(np.cumsum(x[1:] / (1 - x[0])), b, side="right")
            m = m+1
            x[m] -= 1./N  # shift by one since m is selected from queue 1-buffer_size
            x[m-1] += 1. / N
            y = 1

        elif a < y * mu / rates_sum:  # transmission completion if activity protocol was active; indep. of x
            # print(y * mu / rates_sum)
            y = 0
            # t += np.random.exponential(1/mu)
        else:
            # add a job : we choose a random queue
            b = np.random.random()
            m = np.searchsorted(np.cumsum(x), b, side="right")
            if m + 1 < buffer_size:  # discard job if buffer full
                x[m + 1] += 1. / N
                x[m] -= 1. / N

        t += np.random.exponential(1/rates_sum)


        # if verbose and time_index % 100000 == 0:
        #     print(time_index)

        times[time_index] = t
        states[time_index] = x
        y_states[time_index] = y

        time_index += 1

    return times, (states, y_states)


# todo: add seed and verbose(?) option
@jit(nopython=True)
def jit_standart_simu(x0, y0, nu, mu, _lambda, N, G, buffer_size, classes, steps=1e6, seed_nr=-1, verbose=False):
    t = 0.
    _x = x0.copy()  # copy
    _y = y0.copy()  # copy

    # time = N * time  # time scaling
    steps = int(steps)

    times = List()  # initialize as numba List

    ind = 0

    states_x = np.empty(shape=(steps, classes, buffer_size), dtype=np.float_)  # specify array types and length
    states_y = np.empty(shape=(steps, classes), dtype=np.float_)

    times.append(t)  # copy
    states_x[ind] = _x.copy()  # copy
    states_y[ind] = _y.copy()  # copy

    # possible_y_states = self.get_independent_sets(self.G)
    # possible_y_states = [list(y) for y in possible_y_states]

    while ind < steps:
        ##### specify transitions

        valid_transitions = valid_backoffs(y_state=_y, G=G)
        nr_transitions = classes * buffer_size + valid_transitions.sum() * buffer_size + _y.sum()
        nr_transitions = int(nr_transitions)

        transitions_x = np.empty(shape=(nr_transitions, classes, buffer_size), dtype=np.float_)
        transitions_y_from = np.empty(shape=(nr_transitions, classes), dtype=np.float_)
        transitions_y_to = np.empty(shape=(nr_transitions, classes), dtype=np.float_)
        transition_rates = np.empty(shape=nr_transitions, dtype=np.float_)

        ind_3 = 0

        # arrivals (increase queue length of a random server of class c)
        for c in np.arange(classes):  # c * buffersize transitions
            for i in np.arange(0, buffer_size):
                # arrival at server of class c with queue size i
                if i == 0:
                    ell = e(c, i, classes, buffer_size)
                    rate = _lambda[c] * (1 - _x[c, 0])
                    transitions_x[ind_3] = ell
                    transitions_y_from[ind_3] = _y
                    transitions_y_to[ind_3] = _y
                    transition_rates[ind_3] = rate
                    ind_3 += 1
                elif 0 < i < buffer_size:
                    ell = e(c, i, classes, buffer_size)
                    rate = _lambda[c] * (_x[c, i - 1] - _x[c, i])
                    transitions_x[ind_3] = ell
                    transitions_y_from[ind_3] = _y
                    transitions_y_to[ind_3] = _y
                    transition_rates[ind_3] = rate
                    ind_3 += 1
                elif i == buffer_size - 1:
                    # no arrival if buffer is full
                    ell = np.zeros(shape=e(c, i, classes, buffer_size).shape, dtype=np.float_)
                    rate = _lambda[c] * _x[c, i]
                    transitions_x[ind_3] = ell
                    transitions_y_from[ind_3] = _y
                    transitions_y_to[ind_3] = _y
                    transition_rates[ind_3] = rate
                    ind_3 += 1

        # backoff (reduce queue length of server of class c and activate node)
        for c in range(classes):  # valid_transitions.sum() * buffer_size transitions
            if valid_transitions[c]:  # check if backoff to node c from current y state is possible
                for i in range(0, buffer_size):
                    # back off transition of server i (class c) to node c
                    ell = - e(c, i, classes, buffer_size)
                    y_prime = _y.copy()
                    y_prime[c] = 1
                    if i == buffer_size - 1:
                        rate = nu[c] * _x[c, i]
                    else:
                        rate = nu[c] * (_x[c, i] - _x[c, i + 1])
                    transitions_x[ind_3] = ell
                    transitions_y_from[ind_3] = _y
                    transitions_y_to[ind_3] = y_prime
                    transition_rates[ind_3] = rate
                    ind_3 += 1

        # transmission completion y[c] = 1 -> y[c] = 0
        for c in range(classes):  # y.sum() transitions
            if _y[c]:
                ell = np.zeros(shape=(classes, buffer_size))
                y_prime = _y.copy()  # changed from copy(y) to y
                y_prime[c] = 0
                rate = mu[c]
                transitions_x[ind_3] = ell
                transitions_y_from[ind_3] = _y
                transitions_y_to[ind_3] = y_prime
                transition_rates[ind_3] = rate
                ind_3 += 1

        # return transitions_x, transitions_y_from, transitions_y_to, transition_rates


        #####

        # transitions_x, transitions_y_from, transitions_y_to, transition_rates = get_transitions(_x, _y, nu, mu, _lambda, classes, buffer_size, G)

        rates_sum = np.sum(transition_rates)
        a = np.random.random() * rates_sum

        for transition_ind in range(len(transitions_x)):
            if a > transition_rates[transition_ind]:
                a -= transition_rates[transition_ind]
            else:
                break

        _x += transitions_x[transition_ind] / N  # add \ell to the current state
        _y = transitions_y_to[transition_ind]  # change y to new state

        t += np.random.exponential(1 / rates_sum)

        times.append(t)  # copy
        states_x[ind] = _x  # copy
        states_y[ind] = _y  # copy

        ind += 1

    # times = np.array(times)
    # times /= N
    # states_x = np.array(states_x)
    # states_y = np.array(states_y)
    return times, (states_x, states_y)

def fast_simu(x0, y0, nu, mu, _lambda, N, G, buffer_size, classes, steps=1e6, seed_nr=-1, verbose=False):
    # wrapper for jit simulation
    # ensures all inputs are in the right format

    x0 = np.array(x0, dtype=np.float_)
    y0 = np.array(y0, dtype=np.float_)

    nu = np.array(nu, dtype=np.float_)
    mu = np.array(mu, dtype=np.float_)
    _lambda = np.array(_lambda, dtype=np.float_)

    G = np.array(G, dtype=np.float_)

    buffer_size = int(buffer_size)
    classes = int(classes)
    steps = int(steps)

    return jit_standart_simu(x0, y0, nu, mu, _lambda, N, G, buffer_size, classes, steps=steps, seed_nr=seed_nr,
                             verbose=verbose)


if __name__ == "__main__":
    N = 50
    _lambda = np.array([0.5, 0.7, 0.7], dtype=np.float_)
    mu = np.array([10., 10., 10.], dtype=np.float_)
    nu = np.array([3., 3., 3.], dtype=np.float_)

    # Node graph structure, here linear o-o-o
    G = np.array([[0, 1., 0],
                  [1., 0, 1.],
                  [0, 1., 0]], dtype=np.float_)

    classes = G.shape[0]
    buffer_size = 7

    x = np.zeros(shape=(3, 7), dtype=np.float_)
    y = np.array([0., 0., 0.], dtype=np.float_)

    # transitions, rates = get_transitions(x, y, nu, mu, _lambda, classes, buffer_size, G)
    T, (X, Y) = fast_simu(x, y, nu, mu, _lambda, N, G, buffer_size, classes, steps=1e6)
