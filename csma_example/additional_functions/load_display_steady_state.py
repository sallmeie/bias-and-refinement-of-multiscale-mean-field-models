import os
import numpy as np

def print_available_configurations(path):

    configs = {}

    ind = 0

    list_N = []

    print('Available Configurations -- copy and paste to define model --\n\n##### \n')


    def print_copy_paste_specs(specs):
        print('G = np.array(' + np.array2string(specs['G'], separator=",") + ')')
        print('_lambda = np.array(' + np.array2string(np.array(specs['lambda']), separator=",") + ')')
        print('nu = np.array(' + np.array2string(np.array(specs['nu']), separator=",") + ')')
        print('mu = np.array(' + np.array2string(np.array(specs['mu']), separator=",") + ')')
        print('buffer_size = ' + str(specs['buffer_size']))


    printed_configs = []

    for file in os.listdir(path):
        if file[-4:] == '.npy' and file[-8:-4] != 'conf':

            data = np.load(path + file, allow_pickle=True).item()

            specs = data['specs']

            specs.pop('N')

            printed = False
            for configs in printed_configs:
                if np.array_equal(specs['G'], configs['G']):
                    if np.array_equal(specs['lambda'], configs['lambda']):
                        if np.array_equal(specs['mu'], configs['mu']):
                            if np.array_equal(specs['nu'], configs['nu']):
                                if np.array_equal(specs['buffer_size'], configs['buffer_size']):
                                    printed = True
            if not printed:
                printed_configs.append(specs)

                print_copy_paste_specs(specs)

                configs['specs_{}'.format(ind)] = specs
                simu_keys = list(data.keys())
                simu_keys.remove('specs')
                max_ind = max(simu_keys)
                # print('Nr Simulations: ', max_ind)

                print('\n#####\n')

                ind += 1


def get_simulation_filenames(path, csma_model, verbose=True):
    simus = []
    files = []
    files_found = False

    for file in os.listdir(path):
        if file[-4:] == '.npy' and file[-8:-4] != 'conf':
            data = np.load(path + file, allow_pickle=True).item()

            specs = data['specs']


            if np.array_equal(specs['G'], csma_model.G):
                if np.array_equal(specs['lambda'], csma_model._lambda):
                    if np.array_equal(specs['mu'], csma_model.mu):
                        if np.array_equal(specs['nu'], csma_model.nu):
                            if np.array_equal(specs['buffer_size'], csma_model.buffer_size):
                                simus.append(data)
                                files.append(file)
                                if not files_found:
                                    files_found = True
                                    print('Savefiles with the same parameter configuration:\n')
                                if verbose:
                                    simu_keys = list(data.keys())
                                    simu_keys.remove('specs')
                                    max_ind = max(simu_keys) + 1
                                    print('N: ', data['specs']['N'], ' nr_simus: ', max_ind, '  --', end='')

                                    print('\t', file)
                                else:
                                    print('\t', file)

    if not files_found:
        print('No Savefiles were found.')
        return []

    print('\n\nfiles = ', files)

    return simus


def calculate_mean_var(simus):
    mean_dict = {}
    var_dict = {}
    list_N = []
    dict_nr_simus = {}

    for data in simus:
        simu_keys = list(data.keys())
        simu_keys.remove('specs')
        max_ind = max(simu_keys) + 1
        mean_avg_X, var_avg_X = 0, 0
        for _i in range(max_ind):
            mean_avg_X += data[_i]
        mean_avg_X /= max_ind
        for _i in range(max_ind):
            var_avg_X += np.square(data[_i] - mean_avg_X)
        var_avg_X /= (max_ind - 1)

        mean_dict[data['specs']['N']] = mean_avg_X
        var_dict[data['specs']['N']] = var_avg_X
        list_N.append(data['specs']['N'])
        dict_nr_simus[data['specs']['N']] = max_ind

    return mean_dict, var_dict, list_N, dict_nr_simus

def calculate_mean_var_avg_queue(simus):
    mean_dict = {}
    var_dict = {}
    list_N = []
    dict_nr_simus = {}

    for data in simus:
        simu_keys = list(data.keys())
        simu_keys.remove('specs')
        max_ind = max(simu_keys) + 1
        mean_avg_X, var_avg_X = 0, 0
        for _i in range(max_ind):
            mean_avg_X += data[_i]
        mean_avg_X /= max_ind
        mean_avg_X = np.sum(mean_avg_X, axis=1)
        for _i in range(max_ind):
            var_avg_X += np.square(np.sum(data[_i], axis=1) - mean_avg_X)
        var_avg_X /= (max_ind - 1)

        mean_dict[data['specs']['N']] = mean_avg_X
        var_dict[data['specs']['N']] = var_avg_X
        list_N.append(data['specs']['N'])
        dict_nr_simus[data['specs']['N']] = max_ind

    return mean_dict, var_dict, list_N, dict_nr_simus

